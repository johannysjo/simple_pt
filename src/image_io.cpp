#include "image_io.h"

#define STBI_ONLY_HDR
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdio>

namespace {

float clamp(const float x, const float min_value, const float max_value)
{
    return std::fmax(min_value, std::fmin(max_value, x));
}

} // namespace

namespace spt {

// Portable PixMap (PPM) image writer
// Reference: http://netpbm.sourceforge.net/doc/ppm.html
void write_ppm(const char *filename, const std::vector<glm::vec3> &image, const int32_t width,
               const int32_t height)
{
    assert(width > 0);
    assert(height > 0);
    assert(image.size() == uint64_t(width) * height);

    std::FILE *fp = std::fopen(filename, "w");
    if (fp == nullptr) {
        return;
    }

    // Write header
    std::fprintf(fp, "P3\n");
    std::fprintf(fp, "%d %d\n", width, height);
    std::fprintf(fp, "255\n");

    // Write image data. PPM stores pixels in left to right, top to bottom order.
    for (int32_t j = height - 1; j >= 0; --j) {
        for (int32_t i = 0; i < width; ++i) {
            const glm::vec3 &color = image[i + j * width];
            const int32_t ir = int32_t(clamp(256.0f * color.r, 0.0f, 255.0f));
            const int32_t ig = int32_t(clamp(256.0f * color.g, 0.0f, 255.0f));
            const int32_t ib = int32_t(clamp(256.0f * color.b, 0.0f, 255.0f));
            std::fprintf(fp, "%d %d %d\n", ir, ig, ib);
        }
    }

    std::fclose(fp);
}

// Portable FloatMap (PFM) image writer
// Reference: http://www.pauldebevec.com/Research/HDR/PFM/
void write_pfm(const char *filename, const std::vector<glm::vec3> &image, const int32_t width,
               const int32_t height)
{
    assert(width > 0);
    assert(height > 0);
    assert(image.size() == uint64_t(width) * height);

    std::FILE *fp = std::fopen(filename, "wb");
    if (fp == nullptr) {
        return;
    }

    // Write header
    std::fprintf(fp, "PF\n");
    std::fprintf(fp, "%d %d\n", width, height);
    std::fprintf(fp, "-1.0\n"); // indicates little-endian byte order

    // Write image data. PFM stores pixels in left to right, bottom to top order.
    for (int32_t j = 0; j < height; ++j) {
        for (int32_t i = 0; i < width; ++i) {
            const glm::vec3 color = image[i + j * width];
            std::fwrite(&color[0], 1, sizeof(color), fp);
        }
    }

    std::fclose(fp);
}

void load_hdr_image(const char *filename, std::vector<glm::vec3> &image, uint32_t &width,
                    uint32_t &height)
{
    assert(image.empty());

    int32_t width_i32, height_i32, num_channels_in_file;
    const int32_t desired_num_channels = 3;
    float *data =
        stbi_loadf(filename, &width_i32, &height_i32, &num_channels_in_file, desired_num_channels);
    assert(data != nullptr);
    assert(width_i32 > 0);
    assert(height_i32 > 0);
    assert(num_channels_in_file > 0);

    width = uint32_t(width_i32);
    height = uint32_t(height_i32);
    const uint32_t num_channels = uint32_t(desired_num_channels);
    const uint32_t num_pixels = width * height;
    image.reserve(num_pixels);
    for (uint32_t i = 0; i < num_pixels; ++i) {
        const float r = data[num_channels * i];
        const float g = data[num_channels * i + 1];
        const float b = data[num_channels * i + 2];
        image.push_back({r, g, b});
    }
}

} // namespace spt
