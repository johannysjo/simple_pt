#include <core.h>

#include <glm/geometric.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/mat3x3.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>

namespace spt {

Ray transform_ray(const Ray &ray, const glm::mat4 &transform)
{
    Ray transformed_ray{};
    transformed_ray.origin = glm::vec3(transform * glm::vec4(ray.origin, 1.0));
    transformed_ray.direction = glm::mat3(transform) * ray.direction;
    transformed_ray.t_min = ray.t_min;
    transformed_ray.t_max = ray.t_max;

    return transformed_ray;
}

static glm::vec3 sample_sphere(const float u, const float v)
{
    const float z = 2.0f * u - 1.0f;
    const float phi = 2.0f * glm::pi<float>() * v;
    const float r = std::sqrt(1.0f - z * z);
    const float x = r * std::cos(phi);
    const float y = r * std::sin(phi);

    return {x, y, z};
}

static glm::vec2 sample_disk(const float u, const float v)
{
    const float r = std::sqrt(u);
    const float phi = 2.0f * glm::pi<float>() * v;
    const float x = r * std::cos(phi);
    const float y = r * std::sin(phi);

    return {x, y};
}

void camera_init(Camera &camera, const glm::vec3 &eye, const glm::vec3 &up, const glm::vec3 &center,
                 const float vfov, const float aspect, const float aperture, const float focus_dist)
{
    const float half_height = std::tan(vfov / 2.0f);
    const float half_width = aspect * half_height;

    camera.forward = glm::normalize(eye - center);
    camera.side = glm::normalize(glm::cross(glm::normalize(up), camera.forward));
    camera.up = glm::normalize(glm::cross(camera.forward, camera.side));

    camera.origin = eye;
    camera.lower_left_corner =
        camera.origin -
        focus_dist * (half_width * camera.side + half_height * camera.up + camera.forward);
    camera.horizontal = 2.0f * half_width * focus_dist * camera.side;
    camera.vertical = 2.0f * half_height * focus_dist * camera.up;

    camera.lens_radius = aperture / 2.0f;
}

Ray get_camera_ray(const Camera &camera, const float x, const float y, const float rnd_lens_x,
                   const float rnd_lens_y)
{
    const glm::vec2 lens_offset = camera.lens_radius * sample_disk(rnd_lens_x, rnd_lens_y);
    const glm::vec3 offset = lens_offset.x * camera.side + lens_offset.y * camera.up;

    Ray ray{};
    ray.origin = camera.origin + offset;
    ray.direction = glm::normalize(camera.lower_left_corner + x * camera.horizontal +
                                   y * camera.vertical - camera.origin - offset);

    return ray;
}

bool sphere_hit(const Sphere &sphere, const Ray &ray, const float t_min, const float t_max,
                HitInfo &hit_info)
{
    const glm::vec3 oc = ray.origin - sphere.center;
    const float a = glm::dot(ray.direction, ray.direction);
    const float b = glm::dot(oc, ray.direction);
    const float c = glm::dot(oc, oc) - sphere.radius * sphere.radius;
    const float discriminant = b * b - a * c;

    if (discriminant > 0.0f) {
        float t = (-b - std::sqrt(discriminant)) / a;
        if (t < t_max && t > t_min) {
            hit_info.t = t;
            return true;
        }

        t = (-b + std::sqrt(discriminant)) / a;
        if (t < t_max && t > t_min) {
            hit_info.t = t;
            return true;
        }
    }

    return false;
}

glm::vec3 cube_get_normal_at_point(const Cube &cube, const glm::vec3 &p)
{
    const glm::vec3 pc = p - cube.center;
    const glm::vec3 abs_pc = glm::abs(pc);
    glm::vec3 normal{};
    if (abs_pc.x > abs_pc.y && abs_pc.x > abs_pc.z) {
        normal = glm::vec3(pc.x, 0.0f, 0.0f);
    }
    else if (abs_pc.y > abs_pc.z) {
        normal = glm::vec3(0.0f, pc.y, 0.0f);
    }
    else {
        normal = glm::vec3(0.0f, 0.0f, pc.z);
    }

    return normal;
}

bool cube_hit(const Cube &cube, const Ray &ray, float t_min, float t_max, HitInfo &hit_info)
{
    const glm::vec3 min_corner = cube.center - cube.size;
    const glm::vec3 max_corner = cube.center + cube.size;
    const glm::vec3 r_direction_inv = 1.0f / ray.direction;

    const float t_x1 = (min_corner.x - ray.origin.x) * r_direction_inv.x;
    const float t_x2 = (max_corner.x - ray.origin.x) * r_direction_inv.x;
    t_min = std::fmaxf(t_min, std::fminf(t_x1, t_x2));
    t_max = std::fminf(t_max, std::fmaxf(t_x1, t_x2));

    const float t_y1 = (min_corner.y - ray.origin.y) * r_direction_inv.y;
    const float t_y2 = (max_corner.y - ray.origin.y) * r_direction_inv.y;
    t_min = std::fmaxf(t_min, std::fminf(t_y1, t_y2));
    t_max = std::fminf(t_max, std::fmaxf(t_y1, t_y2));

    const float t_z1 = (min_corner.z - ray.origin.z) * r_direction_inv.z;
    const float t_z2 = (max_corner.z - ray.origin.z) * r_direction_inv.z;
    t_min = std::fmaxf(t_min, std::fminf(t_z1, t_z2));
    t_max = std::fminf(t_max, std::fmaxf(t_z1, t_z2));

    hit_info.t = t_min;

    return t_max >= t_min;
}

bool aabb_hit(const AABB &aabb, const Ray &ray, float t_min, float t_max)
{
    const glm::vec3 r_direction_inv = 1.0f / ray.direction;

    const float t_x1 = (aabb.min_corner.x - ray.origin.x) * r_direction_inv.x;
    const float t_x2 = (aabb.max_corner.x - ray.origin.x) * r_direction_inv.x;
    t_min = std::fmaxf(t_min, std::fminf(t_x1, t_x2));
    t_max = std::fminf(t_max, std::fmaxf(t_x1, t_x2));

    const float t_y1 = (aabb.min_corner.y - ray.origin.y) * r_direction_inv.y;
    const float t_y2 = (aabb.max_corner.y - ray.origin.y) * r_direction_inv.y;
    t_min = std::fmaxf(t_min, std::fminf(t_y1, t_y2));
    t_max = std::fminf(t_max, std::fmaxf(t_y1, t_y2));

    const float t_z1 = (aabb.min_corner.z - ray.origin.z) * r_direction_inv.z;
    const float t_z2 = (aabb.max_corner.z - ray.origin.z) * r_direction_inv.z;
    t_min = std::fmaxf(t_min, std::fminf(t_z1, t_z2));
    t_max = std::fminf(t_max, std::fmaxf(t_z1, t_z2));

    return t_max >= t_min;
}

// TODO: replace this with the (much faster) Moeller-Trumbore ray-triangle intesection test
bool triangle_hit(const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, const Ray &ray,
                  const float t_min, const float t_max, HitInfo &hit_info)
{
    const glm::vec3 v0v1 = v1 - v0;
    const glm::vec3 v1v2 = v2 - v1;
    const glm::vec3 v2v0 = v0 - v2;
    const glm::vec3 normal = glm::normalize(glm::cross(v0v1, v1v2));

    // Find the intersection point between the ray and the plane defined by one of the triangle
    // vertices and the triangle normal
    const float t =
        std::fmaxf(0.0f, glm::dot(normal, v0 - ray.origin) / glm::dot(normal, ray.direction));
    const glm::vec3 p = ray.origin + t * ray.direction;

    // Check if the intersection point is inside the triangle
    if (t <= t_max && t > t_min && glm::dot(normal, glm::cross(v0v1, p - v0)) >= 0.0f &&
        glm::dot(normal, glm::cross(v1v2, p - v1)) >= 0.0f &&
        glm::dot(normal, glm::cross(v2v0, p - v2)) >= 0.0f) {
        hit_info.t = t;
        hit_info.p = p;
        hit_info.normal = normal;
        return true;
    }

    return false;
}

bool triangle_mesh_hit(const TriangleMesh &mesh, const Ray &ray, const float t_min, float t_max,
                       HitInfo &hit_info)
{
    bool hit = false;
    HitInfo tmp_hit_info{};
    const uint32_t num_indices = mesh.indices.size();
    for (uint32_t i = 0; i < num_indices; i += 3) {
        const glm::vec3 &v0 = mesh.vertices[mesh.indices[i]];
        const glm::vec3 &v1 = mesh.vertices[mesh.indices[i + 1]];
        const glm::vec3 &v2 = mesh.vertices[mesh.indices[i + 2]];

        if (triangle_hit(v0, v1, v2, ray, t_min, t_max, tmp_hit_info)) {
            hit = true;
            t_max = tmp_hit_info.t;
            hit_info.t = tmp_hit_info.t;
            hit_info.p = tmp_hit_info.p;
            hit_info.normal = tmp_hit_info.normal;
        }
    }

    return hit;
}

AABB compute_aabb(const std::vector<glm::vec3> &vertices)
{
    const uint32_t num_vertices = vertices.size();
    glm::vec3 min_corner = num_vertices > 0 ? vertices[0] : glm::vec3(0.0f);
    glm::vec3 max_corner = num_vertices > 0 ? vertices[0] : glm::vec3(0.0f);
    for (uint32_t i = 1; i < num_vertices; ++i) {
        min_corner = glm::min(min_corner, vertices[i]);
        max_corner = glm::max(max_corner, vertices[i]);
    }

    return {min_corner, max_corner};
}

static glm::vec3 reflect(const glm::vec3 &I, const glm::vec3 &N)
{
    return I - 2.0f * glm::dot(I, N) * N;
}

static bool refract(const glm::vec3 &I, const glm::vec3 &N, const float eta, glm::vec3 &R)
{
    const float N_dot_I = glm::dot(N, I);
    const float k = 1.0f - eta * eta * (1.0f - N_dot_I * N_dot_I);
    if (k > 0.0f) {
        R = eta * I - (eta * N_dot_I + std::sqrt(k)) * N;
        return true;
    }
    else {
        R = glm::vec3(0.0f);
        return false;
    }
}

static float fresnel_schlick(const float refraction_index, const float cosine)
{
    float r0 = (1.0f - refraction_index) / (1.0f + refraction_index);
    r0 *= r0;
    return r0 + (1.0f - r0) * std::pow(1.0f - cosine, 5.0f);
}

static float fresnel_schlick_r0(const float r0, const float cosine)
{
    return r0 + (1.0f - r0) * std::pow(1.0f - cosine, 5.0f);
}

static bool material_matte_scatter(const Material &material, const HitInfo &hit_info,
                                   const float rnd_bsdf_x, const float rnd_bsdf_y,
                                   glm::vec3 &attenuation, Ray &scattered)
{
    scattered.origin = hit_info.p;
    scattered.direction =
        glm::normalize(hit_info.normal + 0.999f * sample_sphere(rnd_bsdf_x, rnd_bsdf_y));
    attenuation = material.albedo;

    return true;
}

static bool material_metal_scatter(const Material &material, const Ray &ray,
                                   const HitInfo &hit_info, const float rnd_bsdf_x,
                                   const float rnd_bsdf_y, glm::vec3 &attenuation, Ray &scattered)
{
    const glm::vec3 I = glm::normalize(ray.direction);
    const glm::vec3 N = hit_info.normal;
    const glm::vec3 R = reflect(I, N);

    const float roughness_squared = material.roughness * material.roughness;

    scattered.origin = hit_info.p;
    scattered.direction =
        glm::normalize(R + roughness_squared * sample_sphere(rnd_bsdf_x, rnd_bsdf_y));
    attenuation = material.albedo;

    return glm::dot(scattered.direction, hit_info.normal) > 0.0f;
}

static bool material_glass_scatter(const Material &material, const Ray &ray,
                                   const HitInfo &hit_info, const float rnd_fresnel,
                                   glm::vec3 &attenuation, Ray &scattered)
{
    const glm::vec3 I = glm::normalize(ray.direction);
    const glm::vec3 N = hit_info.normal;
    const glm::vec3 reflected = reflect(I, N);

    glm::vec3 outward_normal{};
    float ni_over_nt = 0.0f;
    float cosine = 0.0f;
    const float I_dot_N = glm::dot(I, N);
    if (I_dot_N > 0.0f) {
        outward_normal = -N;
        ni_over_nt = material.refraction_index;
        cosine = material.refraction_index * I_dot_N / glm::length(I);
    }
    else {
        outward_normal = N;
        ni_over_nt = 1.0f / material.refraction_index;
        cosine = -I_dot_N / glm::length(I);
    }

    glm::vec3 refracted{};
    const float F = refract(I, outward_normal, ni_over_nt, refracted)
                        ? fresnel_schlick(material.refraction_index, cosine)
                        : 1.0f;

    scattered.origin = hit_info.p;
    scattered.direction = rnd_fresnel < F ? reflected : refracted;
    attenuation = glm::vec3(1.0f);

    return true;
}

static bool material_glossy_scatter(const Material &material, const Ray &ray,
                                    const HitInfo &hit_info, const float rnd_bsdf_x,
                                    const float rnd_bsdf_y, const float rnd_fresnel,
                                    glm::vec3 &attenuation, Ray &scattered)
{
    const glm::vec3 I = glm::normalize(ray.direction);
    const glm::vec3 N = hit_info.normal;
    const glm::vec3 R = reflect(I, N);

    const float r0 = 0.04f;
    const float cosine = std::fmaxf(0.0f, glm::dot(-I, N));
    const float F = fresnel_schlick_r0(r0, cosine);

    scattered.origin = hit_info.p;
    if (rnd_fresnel < F) {
        const float roughness_squared = material.roughness * material.roughness;
        scattered.direction =
            glm::normalize(R + roughness_squared * sample_sphere(rnd_bsdf_x, rnd_bsdf_y));
        attenuation = glm::vec3(1.0f);
    }
    else {
        scattered.direction = glm::normalize(N + 0.999f * sample_sphere(rnd_bsdf_x, rnd_bsdf_y));
        attenuation = material.albedo;
    }

    return glm::dot(scattered.direction, hit_info.normal) > 0.0f;
}

bool material_scatter(const Material &material, const Ray &ray, const HitInfo &hit_info,
                      const float rnd_bsdf_x, const float rnd_bsdf_y, const float rnd_fresnel,
                      glm::vec3 &attenuation, Ray &scattered)
{
    if (material.type == MATERIAL_TYPE_MATTE) {
        return material_matte_scatter(material, hit_info, rnd_bsdf_x, rnd_bsdf_y, attenuation,
                                      scattered);
    }
    else if (material.type == MATERIAL_TYPE_METAL) {
        return material_metal_scatter(material, ray, hit_info, rnd_bsdf_x, rnd_bsdf_y, attenuation,
                                      scattered);
    }
    else if (material.type == MATERIAL_TYPE_GLASS) {
        return material_glass_scatter(material, ray, hit_info, rnd_fresnel, attenuation, scattered);
    }
    else if (material.type == MATERIAL_TYPE_GLOSSY) {
        return material_glossy_scatter(material, ray, hit_info, rnd_bsdf_x, rnd_bsdf_y, rnd_fresnel,
                                       attenuation, scattered);
    }
    else if (material.type == MATERIAL_TYPE_EMISSIVE) {
        return false;
    }
    return false;
}

glm::vec3 material_emit(const Material &material)
{
    if (material.type == MATERIAL_TYPE_EMISSIVE) {
        return material.albedo * material.intensity;
    }
    else {
        return {0.0f, 0.0f, 0.0f};
    }
}

glm::vec3 sample_hemisphere_light(const HemisphereLight &light, const Ray &world_ray)
{
    const float w = 0.5f * glm::normalize(world_ray.direction).y + 0.5f;
    return glm::mix(light.ground_color, light.sky_color, w);
}

static glm::vec3 get_pixel_value(const std::vector<glm::vec3> &image, const uint32_t width,
                                 const uint32_t height, const glm::uvec2 &pixel_pos)
{
    assert(width > 0);
    assert(height > 0);
    assert(image.size() == width * height);

    const uint32_t i = pixel_pos[0] < width ? pixel_pos[0] : width - 1;
    const uint32_t j = pixel_pos[1] < height ? pixel_pos[1] : height - 1;
    const uint32_t pixel_index = i + j * width;

    return image[pixel_index];
}

static glm::vec3 get_interp_pixel_value(const std::vector<glm::vec3> &image, const uint32_t width,
                                        const uint32_t height, const glm::vec2 &local_pos)
{
    assert(width > 0);
    assert(height > 0);
    assert(image.size() == width * height);

    const glm::vec2 local_pos_01 = glm::mod(local_pos, 1.0f);
    const glm::uvec2 pixel_pos = {std::floor(local_pos_01[0] * width),
                                  std::floor(local_pos_01[1] * height)};

    const float xd = std::fmod(local_pos_01[0] * width, 1.0f);
    const float yd = std::fmod(local_pos_01[1] * height, 1.0f);

    const glm::vec3 c00 = get_pixel_value(image, width, height, pixel_pos);
    const glm::vec3 c01 = get_pixel_value(image, width, height, pixel_pos + glm::uvec2(0, 1));
    const glm::vec3 c10 = get_pixel_value(image, width, height, pixel_pos + glm::uvec2(1, 0));
    const glm::vec3 c11 = get_pixel_value(image, width, height, pixel_pos + glm::uvec2(1, 1));

    const glm::vec3 c0 = c00 * (1.0f - xd) + c10 * xd;
    const glm::vec3 c1 = c01 * (1.0f - xd) + c11 * xd;
    const glm::vec3 interp_color = c0 * (1.0f - yd) + c1 * yd;

    return interp_color;
}

glm::vec3 sample_envmap(const std::vector<glm::vec3> &image, const uint32_t width,
                        const uint32_t height, const glm::vec3 &ray_dir)
{
    assert(width > 0);
    assert(height > 0);
    assert(image.size() == width * height);

    // Convert the 3D ray direction to a 2D equirectangular texture coordinate
    const float phi = std::asin(ray_dir[1]);
    const float theta = std::atan2(ray_dir[0], ray_dir[2]);
    const float s = 0.5f * theta / glm::pi<float>() + 0.5f;
    const float t = phi / glm::pi<float>() + 0.5f;

    // Sample the environment map
    const glm::vec2 local_pos = {1.0f - s, 1.0f - t};
    const glm::vec3 color = get_interp_pixel_value(image, width, height, local_pos);

    return color;
}

glm::vec3 sample_sky_light(const SkyLight &sky_light, const Ray &world_ray)
{
    if (sky_light.type == SKY_LIGHT_TYPE_HEMISPHERE) {
        return sky_light.intensity * sample_hemisphere_light(sky_light.hemisphere_light, world_ray);
    }
    else if (sky_light.type == SKY_LIGHT_TYPE_ENVMAP) {
        return sky_light.intensity * sample_envmap(sky_light.envmap.pixels, sky_light.envmap.width,
                                                   sky_light.envmap.height,
                                                   glm::normalize(world_ray.direction));
    }
    else {
        return {0.0f, 0.0f, 0.0f};
    }
}

} // namespace spt
