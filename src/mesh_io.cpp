#include "mesh_io.h"

#include <cstdio>

namespace spt {

void load_obj_mesh(const char *filename, std::vector<glm::vec3> &vertices,
                   std::vector<uint32_t> &indices)
{
    std::FILE *fp = std::fopen(filename, "r");
    if (fp == nullptr) {
        return;
    }

    char line[256];
    while (std::fgets(line, 256, fp)) {
        char str[2];
        if (line[0] == 'v' && line[1] == ' ') {
            float x, y, z;
            std::sscanf(line, "%s %f %f %f", str, &x, &y, &z);
            vertices.emplace_back(glm::vec3(x, y, z));
        }
        else if (line[0] == 'f' && line[1] == ' ') {
            uint32_t idx0, idx1, idx2;
            std::sscanf(line, "%s %u %u %u", str, &idx0, &idx1, &idx2);
            idx0 -= 1;
            idx1 -= 1;
            idx2 -= 1;
            indices.insert(indices.end(), {idx0, idx1, idx2});
        }
    }

    std::fclose(fp);
}

} // namespace spt
