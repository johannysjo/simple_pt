#include "rng.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/vec2.hpp>

#undef NDEBUG
#include <cassert>
#include <cmath>

namespace {

struct RNGState {
    uint32_t seed;
};

inline uint32_t rand_lcg(RNGState &rng_state)
{
    rng_state.seed = 1664525 * rng_state.seed + 1013904223;
    return rng_state.seed;
}

inline float randf_lcg(RNGState &rng_state)
{
    return float(rand_lcg(rng_state)) / 4294967296.0f;
}

void permute_in_place(std::vector<uint32_t> &values, RNGState &rng_state)
{
    const uint32_t num_values = values.size();
    for (uint32_t i = 0; i < num_values; ++i) {
        const uint32_t j = i + rand_lcg(rng_state) % (num_values - i);
        const auto tmp = values[i];
        values[i] = values[j];
        values[j] = tmp;
    }
}

// Returns the N first prime bases of the Halton sequence, starting from 2.
std::vector<uint32_t> get_halton_bases(const uint32_t num_bases)
{
    // Generating prime numbers is expensive, so we set a reasonable limit here on the maximum
    // number of bases that can be requested
    assert(num_bases < 200);

    auto bases = std::vector<uint32_t>{};
    bases.reserve(num_bases);
    uint32_t base_count = 0;
    uint32_t value = 2;
    while (base_count < num_bases) {
        bool is_prime = true;
        for (uint32_t i = 2; i < value; ++i) {
            if (value % i == 0) {
                is_prime = false;
                break;
            }
        }

        if (is_prime) {
            bases.push_back(value);
            base_count += 1;
        }

        value += 1;
    }

    return bases;
}

float halton_permuted(const uint32_t index, const uint32_t base, const std::vector<uint32_t> &perm)
{
    assert(perm.size() == base);

    float f = 1.0f;
    float result = 0.0f;
    uint32_t i = index;
    while (i > 0) {
        f = f / base;
        result += f * perm[i % base];
        i = uint32_t(std::floor(float(i) / base));
    }

    return result;
}

float hash2d(const glm::vec2 &seed)
{
    return glm::fract(std::sin(glm::dot(seed, glm::vec2(12.9898f, 78.2331f))) * 43758.5453f);
}

float hash3d(const glm::vec3 &seed)
{
    return hash2d(glm::vec2(hash2d(glm::vec2(seed[0], seed[1])), seed[2]));
}

} // namespace

namespace spt {

// This function generates N digit-permuted Halton sequences with M samples per sequence. The
// permutation prevents correlation between higher bases, which is a well-known limitation of the
// original unpermuted Halton sequence.
std::vector<std::vector<float>> generate_permuted_halton_sequences(const uint32_t num_sequences,
                                                                   const uint32_t num_samples,
                                                                   const uint32_t seed)
{
    const std::vector<uint32_t> bases = get_halton_bases(num_sequences);

    RNGState rng_state = {seed};
    std::vector<std::vector<float>> halton_sequences{};
    for (uint32_t i = 0; i < num_sequences; ++i) {
        const uint32_t base = bases[i];

        // Generate permutation table for the current base
        std::vector<uint32_t> permutation_table{};
        permutation_table.reserve(base);
        for (uint32_t digit = 0; digit < base; ++digit) {
            permutation_table.push_back(digit);
        }
        permute_in_place(permutation_table, rng_state);
        assert(permutation_table.size() == base);

        // Calculate permuted Halton sequence
        std::vector<float> halton_sequence_i{};
        halton_sequence_i.reserve(num_samples);
        for (uint32_t j = 0; j < num_samples; ++j) {
            halton_sequence_i.push_back(halton_permuted(j, base, permutation_table));
        }

        halton_sequences.push_back(halton_sequence_i);
    }

    return halton_sequences;
}

std::vector<std::vector<float>> generate_lcg_sequences(const uint32_t num_sequences,
                                                       const uint32_t num_samples,
                                                       const uint32_t seed)
{
    RNGState rng_state = {seed};
    std::vector<std::vector<float>> lcg_sequences{};
    for (uint32_t i = 0; i < num_sequences; ++i) {
        std::vector<float> lcg_sequence_i{};
        lcg_sequence_i.reserve(num_samples);
        for (uint32_t j = 0; j < num_samples; ++j) {
            lcg_sequence_i.push_back(randf_lcg(rng_state));
        }

        lcg_sequences.push_back(lcg_sequence_i);
    }

    return lcg_sequences;
}

std::vector<float> hash3d_sequence(const uint32_t num_samples, const glm::vec3 &seed)
{
    std::vector<float> sequence{};
    sequence.reserve(num_samples);
    for (uint32_t i = 0; i < num_samples; ++i) {
        sequence.push_back(hash3d(glm::vec3(seed[0], seed[1], seed[2] + i)));
    }

    return sequence;
}

} // namespace spt
