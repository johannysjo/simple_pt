#include "utils.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>

#include <cmath>

namespace spt {

glm::vec3 srgb2lin(const glm::vec3 &color)
{
    return glm::pow(color, glm::vec3(2.2f));
}

glm::vec3 hsv2rgb(const float h, const float s, const float v)
{
    const glm::vec3 k = glm::fract(glm::vec3(5.0f, 3.0f, 1.0f) / 6.0f + h) * 6.0f;
    return v - v * s * glm::clamp(glm::min(k, 4.0f - k), 0.0f, 1.0f);
}

glm::vec3 golden_ratio_color(const uint32_t index)
{
    const float golden_ratio = 1.61803399f;
    const float h = std::fmod(index * golden_ratio, 1.0f);
    const float s = 0.45f;
    const float v = 0.8f;

    return hsv2rgb(h, s, v);
}

} // namespace spt
