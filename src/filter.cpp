#include "filter.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>

namespace {

inline glm::vec3 tonemap_aces(const glm::vec3 &color)
{
    const float a = 2.51f;
    const float b = 0.03f;
    const float c = 2.43f;
    const float d = 0.59f;
    const float e = 0.14f;

    return glm::clamp((color * (a * color + b)) / (color * (c * color + d) + e), 0.0f, 1.0f);
}

// http://chilliant.blogspot.com.au/2012/08/srgb-approximations-for-hlsl.html
inline glm::vec3 lin2srgb(const glm::vec3 &rgb)
{
    return glm::max(1.055f * glm::pow(rgb, glm::vec3(0.416666667f)) - 0.055f, 0.0f);
}

} // namespace

namespace spt {

std::vector<glm::vec3> tonemap_aces_filter(const std::vector<glm::vec3> &image)
{
    auto output_image = std::vector<glm::vec3>{};
    output_image.reserve(image.size());
    for (const auto &color : image) {
        output_image.push_back(tonemap_aces(color));
    }

    return output_image;
}

std::vector<glm::vec3> lin2srgb_filter(const std::vector<glm::vec3> &image)
{
    auto output_image = std::vector<glm::vec3>{};
    output_image.reserve(image.size());
    for (const auto &color : image) {
        output_image.push_back(lin2srgb(color));
    }

    return output_image;
}

} // namespace spt
