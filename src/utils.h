/// @file
/// @brief Utility functions.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include "stdint.h"

namespace spt {

glm::vec3 srgb2lin(const glm::vec3 &color);

glm::vec3 hsv2rgb(float h, float s, float v);

glm::vec3 golden_ratio_color(uint32_t index);

} // namespace spt
