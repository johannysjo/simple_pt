/// @file
/// @brief Image postprocessing filters.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include <vector>

namespace spt {

std::vector<glm::vec3> tonemap_aces_filter(const std::vector<glm::vec3> &image);

std::vector<glm::vec3> lin2srgb_filter(const std::vector<glm::vec3> &image);

} // namespace spt
