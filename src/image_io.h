/// @file
/// @brief Image IO.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace spt {

void write_ppm(const char *filename, const std::vector<glm::vec3> &image, int32_t width,
               int32_t height);

void write_pfm(const char *filename, const std::vector<glm::vec3> &image, int32_t width,
               int32_t height);

void load_hdr_image(const char *filename, std::vector<glm::vec3> &image, uint32_t &width,
                    uint32_t &height);

} // namespace spt
