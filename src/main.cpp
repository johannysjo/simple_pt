/// @file
/// @brief Main entry point for the path tracer.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include "core.h"
#include "filter.h"
#include "image_io.h"
#include "logging.h"
#include "mesh_io.h"
#include "rng.h"
#include "utils.h"

#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <omp.h>

#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <random>
#include <stdint.h>
#include <vector>

#define RNG_IDX_AA_X 0
#define RNG_IDX_AA_Y 1
#define RNG_IDX_LENS_X 2
#define RNG_IDX_LENS_Y 3
#define RNG_NUM_BASE_IDX 4
#define RNG_IDX_BSDF_X 0
#define RNG_IDX_BSDF_Y 1
#define RNG_IDX_FRESNEL 2
#define RNG_NUM_BOUNCE_IDX 3

enum SceneID { SCENE_ID_RANDOM_SPHERES, SCENE_ID_CORNELL_BOX, SCENE_ID_BUNNY };

struct FirstHitInfo {
    glm::vec3 albedo;
    glm::vec3 world_normal;
};

struct Settings {
    SceneID scene_id = SCENE_ID_RANDOM_SPHERES;
    int32_t width = 600;
    int32_t height = 400;
    uint32_t spp = 20;
    uint32_t max_path_depth = 25;
    float exposure = 1.0f;
};

struct Scene {
    std::vector<spt::Sphere> spheres{};
    std::vector<spt::Cube> cubes{};
    std::vector<spt::TriangleMesh> meshes{};
    std::vector<spt::AABB> meshes_aabb{};
    spt::TriangleMeshInstances mesh_instances{};
    std::vector<spt::Material> materials{};
    spt::SkyLight sky_light{};
};

float get_base_rng_sample(const uint32_t sample_idx, const uint32_t base_idx,
                          const std::vector<std::vector<float>> &rng_sequences)
{
    assert(base_idx < RNG_NUM_BASE_IDX);
    return rng_sequences.at(base_idx).at(sample_idx);
}

float get_bounce_rng_sample(const uint32_t sample_idx, const uint32_t path_depth,
                            const uint32_t bounce_idx,
                            const std::vector<std::vector<float>> &rng_sequences)
{
    assert(bounce_idx < RNG_NUM_BOUNCE_IDX);
    const uint32_t sequence_idx = RNG_NUM_BASE_IDX + path_depth * RNG_NUM_BOUNCE_IDX + bounce_idx;
    return rng_sequences.at(sequence_idx).at(sample_idx);
}

// Only used for generating random scene content, not for sampling
float randf()
{
    return float(std::rand()) / float(RAND_MAX);
}

void initialize_random_spheres_scene(Scene &scene)
{
    std::srand(1231);

    // Create a large sphere representing the ground
    uint32_t material_id = 0;
    scene.materials.push_back(spt::Material{
        spt::MATERIAL_TYPE_MATTE, spt::srgb2lin(glm::vec3(0.73f, 0.73f, 0.73f)), 0.0f, 0.0f, 0.0f});
    scene.spheres.push_back(spt::Sphere{glm::vec3(0.0f, -1000.0f, 0.0f), 1000.0f, material_id});

    // Place small random spheres on the ground (in a grid with random offsets)
    for (int32_t i = -3; i < 3; ++i) {
        for (int32_t j = -3; j < 3; ++j) {
            float radius = 0.2f + 0.2f * randf();

            // NOTE: previously randf() was called in the constructor to glm::vec3, which resulted
            // in undefined execution order and different results on GCC and MSVC. So now we have to
            // call randf() for z before x to get the same random scene as before :P
            glm::vec3 center{};
            center.z = float(j) + 0.5f * randf();
            center.y = radius;
            center.x = float(i) + 0.5f * randf();

            int32_t material_type = std::rand() % 4;
            if (material_type == 0) { // matte
                const glm::vec3 albedo = spt::srgb2lin(glm::vec3(0.12f, 0.8f, 0.9f));
                material_id += 1;
                scene.materials.push_back(
                    spt::Material{spt::MATERIAL_TYPE_MATTE, albedo, 0.0f, 0.0f, 0.0f});
                scene.spheres.push_back(spt::Sphere{center, radius, material_id});
            }
            else if (material_type == 1) { // metal
                radius *= 0.5f;
                center.y *= 0.5f;
                const glm::vec3 albedo = spt::srgb2lin(glm::vec3(0.9f, 0.9f, 0.9f));
                const float roughness = 0.55f * randf();
                material_id += 1;
                scene.materials.push_back(
                    spt::Material{spt::MATERIAL_TYPE_METAL, albedo, roughness, 0.0f, 0.0f});
                scene.spheres.push_back(spt::Sphere{center, radius, material_id});
            }
            else if (material_type == 2) { // glass
                radius *= 0.5f;
                center.y *= 0.5f;
                const float refraction_index = 2.0f;
                material_id += 1;
                scene.materials.push_back(spt::Material{spt::MATERIAL_TYPE_GLASS, glm::vec3(0.0f),
                                                        1.0f, refraction_index, 0.0f});
                scene.spheres.push_back(spt::Sphere{center, radius, material_id});
            }
            else if (material_type == 3) { // glossy
                const glm::vec3 albedo = spt::srgb2lin(glm::vec3(0.85f, 0.0f, 0.25f));
                const float roughness = 0.3f * randf();
                material_id += 1;
                scene.materials.push_back(
                    spt::Material{spt::MATERIAL_TYPE_GLOSSY, albedo, roughness, 0.0f, 0.0f});
                scene.spheres.push_back(spt::Sphere{center, radius, material_id});
            }
        }
    }

    spt::PixelBuffer envmap{};
    spt::load_hdr_image("./resources/envmaps/konzerthaus_2k.hdr", envmap.pixels, envmap.width,
                        envmap.height);

    scene.sky_light.type = spt::SKY_LIGHT_TYPE_ENVMAP;
    scene.sky_light.envmap = envmap;
    scene.sky_light.intensity = 0.8f;
}

void initialize_cornell_box_scene(Scene &scene)
{
    scene.materials = {
        spt::Material{spt::MATERIAL_TYPE_MATTE, glm::vec3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f},
        spt::Material{spt::MATERIAL_TYPE_MATTE, glm::vec3(1.0f, 0.0f, 0.0f), 0.0f, 0.0f, 0.0f},
        spt::Material{spt::MATERIAL_TYPE_MATTE, glm::vec3(0.0f, 1.0f, 1.0f), 0.0f, 0.0f, 0.0f},
        spt::Material{spt::MATERIAL_TYPE_EMISSIVE, glm::vec3(1.0f, 1.0f, 1.0f), 0.0f, 0.0f, 4.0f},
        spt::Material{spt::MATERIAL_TYPE_METAL, glm::vec3(1.0f, 1.0f, 1.0f), 0.3f, 0.0f, 0.0f},
        spt::Material{spt::MATERIAL_TYPE_GLASS, glm::vec3(1.0f, 1.0f, 1.0f), 0.0f, 2.5f, 0.0f}};

    scene.spheres = {spt::Sphere{glm::vec3(0.0, -1000.0, 0.0), 1000.0, 0}, // floor
                     spt::Sphere{glm::vec3(-1001.0, 0.0, 0.0), 1000.0, 1}, // left wall
                     spt::Sphere{glm::vec3(1001, 0.0, 0.0), 1000.0, 2},    // right wall
                     spt::Sphere{glm::vec3(0.0, 0.0, -1001.0), 1000.0, 0}, // back wall
                     spt::Sphere{glm::vec3(0.0, 1002, 0.0), 1000.0, 0},    // roof
                     spt::Sphere{glm::vec3(0.0, 0.0, 1005.0), 1000.0, 0},  // front wall
                     spt::Sphere{glm::vec3(0.0, 3.96, 0.0), 2.0, 3},       // light source
                     spt::Sphere{glm::vec3(-0.4, 0.4, -0.4), 0.4, 4},      // metal sphere
                     spt::Sphere{glm::vec3(0.45, 0.4, 0.3), 0.4, 5}};      // glass sphere

    scene.sky_light.type = spt::SKY_LIGHT_TYPE_NONE;
}

void initialize_bunny_scene(Scene &scene)
{
    std::srand(1237);

    // Load bunny mesh
    spt::TriangleMesh mesh{};
    spt::load_obj_mesh("./src/bunny_lowpoly.obj", mesh.vertices, mesh.indices);
    scene.meshes.push_back(mesh);

    const spt::AABB mesh_aabb = spt::compute_aabb(mesh.vertices);
    scene.meshes_aabb.push_back(mesh_aabb);

    // Create a large sphere representing the ground
    uint32_t material_id = 0;
    scene.materials.push_back(spt::Material{
        spt::MATERIAL_TYPE_MATTE, spt::srgb2lin(glm::vec3(0.65f, 0.65f, 0.65f)), 0.0f, 0.0f, 0.0f});
    scene.spheres.push_back(spt::Sphere{glm::vec3(0.0f, -1000.0f, 0.0f), 1000.0f, material_id});

    // Add a few larger bunny instances
    uint32_t instance_id = 0;
    const uint32_t num_large_bunny_instances = 7;
    for (uint32_t i = 0; i < num_large_bunny_instances; ++i) {
        material_id += 1;
        scene.materials.push_back(spt::Material{spt::MATERIAL_TYPE_GLOSSY,
                                                spt::srgb2lin(spt::golden_ratio_color(instance_id)),
                                                0.45f, 0.0f, 0.0f});

        const float scale = 0.6f + 0.2f * randf();
        glm::vec3 position{};
        position.x = 3.0f * (2.0f * randf() - 1.0f);
        position.y = -(scale * mesh_aabb.min_corner.y);
        position.z = 3.0f * (2.0f * randf() - 1.0f);

        const uint32_t mesh_id = 0;
        const glm::mat4 world_from_model = glm::translate(glm::mat4(1.0f), glm::vec3(position)) *
                                           glm::scale(glm::mat4(1.0f), glm::vec3(scale));
        const glm::mat4 model_from_world = glm::inverse(world_from_model);

        scene.mesh_instances.mesh_id.push_back(mesh_id);
        scene.mesh_instances.world_from_model.push_back(world_from_model);
        scene.mesh_instances.model_from_world.push_back(model_from_world);
        scene.mesh_instances.material_id.push_back(material_id);
        instance_id += 1;
    }

    // Add lots of smaller bunny instances
    const uint32_t num_small_bunny_instances = 200;
    for (uint32_t i = 0; i < num_small_bunny_instances; ++i) {
        material_id += 1;
        scene.materials.push_back(spt::Material{spt::MATERIAL_TYPE_GLOSSY,
                                                spt::srgb2lin(spt::golden_ratio_color(instance_id)),
                                                0.45f, 0.0f, 0.0f});

        const float scale = 0.1f + 0.1f * randf();
        glm::vec3 position{};
        position.x = 4.5f * (2.0f * randf() - 1.0f);
        position.y = -(scale * mesh_aabb.min_corner.y);
        position.z = 4.5f * (2.0f * randf() - 1.0f);

        const uint32_t mesh_id = 0;
        const glm::mat4 world_from_model = glm::translate(glm::mat4(1.0f), glm::vec3(position)) *
                                           glm::scale(glm::mat4(1.0f), glm::vec3(scale));
        const glm::mat4 model_from_world = glm::inverse(world_from_model);

        scene.mesh_instances.mesh_id.push_back(mesh_id);
        scene.mesh_instances.world_from_model.push_back(world_from_model);
        scene.mesh_instances.model_from_world.push_back(model_from_world);
        scene.mesh_instances.material_id.push_back(material_id);
        instance_id += 1;
    }

    spt::PixelBuffer envmap{};
    spt::load_hdr_image("./resources/envmaps/konzerthaus_2k.hdr", envmap.pixels, envmap.width,
                        envmap.height);

    scene.sky_light.type = spt::SKY_LIGHT_TYPE_ENVMAP;
    scene.sky_light.envmap = envmap;
    scene.sky_light.intensity = 1.0f;
}

void initialize_scene(Scene &scene, const SceneID scene_id)
{
    if (scene_id == SCENE_ID_RANDOM_SPHERES) {
        initialize_random_spheres_scene(scene);
    }
    else if (scene_id == SCENE_ID_CORNELL_BOX) {
        initialize_cornell_box_scene(scene);
    }
    else if (scene_id == SCENE_ID_BUNNY) {
        initialize_bunny_scene(scene);
    }
    else {
        assert(false && "Unknown scene");
    }
}

void initialize_random_spheres_scene_camera(spt::Camera &camera, const int32_t width,
                                            const int32_t height)
{
    const glm::vec3 eye = {2.0f, 1.0f, 4.0f};
    const glm::vec3 up = {0.0f, 1.0f, 0.0f};
    const glm::vec3 center = {0.0f, 0.0f, 0.0f};
    const float vfov = glm::radians(40.0f);
    const float aspect = float(width) / height;
    const float aperture = 0.12f;
    const float focus_dist = glm::length(eye - center);
    spt::camera_init(camera, eye, up, center, vfov, aspect, aperture, focus_dist);
}

void initialize_cornell_box_scene_camera(spt::Camera &camera, const int32_t width,
                                         const int32_t height)
{
    const glm::vec3 eye = {0.0f, 1.0f, 4.75f};
    const glm::vec3 up = {0.0f, 1.0f, 0.0f};
    const glm::vec3 center = {0.0f, 0.975f, 0.0f};
    const float vfov = glm::radians(30.0f);
    const float aspect = float(width) / height;
    const float aperture = 0.0f;
    const float focus_dist = glm::length(eye - center);
    spt::camera_init(camera, eye, up, center, vfov, aspect, aperture, focus_dist);
}

void initialize_bunny_scene_camera(spt::Camera &camera, const int32_t width, const int32_t height)
{
    const glm::vec3 eye = {0.0f, 1.0f, 6.0f};
    const glm::vec3 up = {0.0f, 1.0f, 0.0f};
    const glm::vec3 center = {0.0f, 0.25f, 0.0f};
    const float vfov = glm::radians(45.0f);
    const float aspect = float(width) / height;
    const float aperture = 0.08f;
    const float focus_dist = 0.5f * glm::length(eye - center);
    spt::camera_init(camera, eye, up, center, vfov, aspect, aperture, focus_dist);
}

void initialize_camera(spt::Camera &camera, const SceneID scene_id, const int32_t width,
                       const int32_t height)
{
    if (scene_id == SCENE_ID_RANDOM_SPHERES) {
        initialize_random_spheres_scene_camera(camera, width, height);
    }
    else if (scene_id == SCENE_ID_CORNELL_BOX) {
        initialize_cornell_box_scene_camera(camera, width, height);
    }
    else if (scene_id == SCENE_ID_BUNNY) {
        initialize_bunny_scene_camera(camera, width, height);
    }
    else {
        assert(false && "Unknown scene");
    }
}

bool hit_anything(const Scene &scene, const spt::Ray &world_ray, spt::HitInfo &hit_info)
{
    spt::HitInfo tmp_hit_info;
    bool hit = false;
    float t_closest_so_far = world_ray.t_max;

    // Spheres
    uint32_t sphere_id = 0;
    const uint32_t num_spheres = scene.spheres.size();
    for (uint32_t i = 0; i < num_spheres; ++i) {
        if (spt::sphere_hit(scene.spheres[i], world_ray, world_ray.t_min, t_closest_so_far,
                            tmp_hit_info)) {
            hit = true;
            t_closest_so_far = tmp_hit_info.t;
            sphere_id = i;
        }
    }
    if (hit) {
        hit_info.t = t_closest_so_far;
        hit_info.p = world_ray.origin + hit_info.t * world_ray.direction;
        hit_info.normal = glm::normalize(hit_info.p - scene.spheres[sphere_id].center);
        hit_info.material_id = scene.spheres[sphere_id].material_id;
    }

    // Cubes
    const uint32_t num_cubes = scene.cubes.size();
    for (uint32_t i = 0; i < num_cubes; ++i) {
        if (spt::cube_hit(scene.cubes[i], world_ray, world_ray.t_min, t_closest_so_far,
                          tmp_hit_info)) {
            hit = true;
            t_closest_so_far = tmp_hit_info.t;
            hit_info.t = tmp_hit_info.t;
            hit_info.p = world_ray.origin + hit_info.t * world_ray.direction;
            hit_info.normal =
                glm::normalize(spt::cube_get_normal_at_point(scene.cubes[i], hit_info.p));
            hit_info.material_id = scene.cubes[i].material_id;
        }
    }

    // Triangle mesh instances
    const uint32_t num_mesh_instances = scene.mesh_instances.mesh_id.size();
    for (uint32_t i = 0; i < num_mesh_instances; ++i) {
        const uint32_t mesh_id = scene.mesh_instances.mesh_id[i];
        const glm::mat4 &model_from_world = scene.mesh_instances.model_from_world[i];
        const spt::Ray model_ray = spt::transform_ray(world_ray, model_from_world);

        if (spt::aabb_hit(scene.meshes_aabb[mesh_id], model_ray, model_ray.t_min,
                          t_closest_so_far) &&
            spt::triangle_mesh_hit(scene.meshes[mesh_id], model_ray, model_ray.t_min,
                                   t_closest_so_far, tmp_hit_info)) {
            hit = true;
            t_closest_so_far = tmp_hit_info.t;
            hit_info.t = tmp_hit_info.t;
            hit_info.p = world_ray.origin + hit_info.t * world_ray.direction;
            hit_info.normal = tmp_hit_info.normal;
            hit_info.material_id = scene.mesh_instances.material_id[i];
        }
    }

    return hit;
}

glm::vec3 trace_ray(spt::Ray &world_ray, const Scene &scene,
                    const std::vector<std::vector<float>> &rng_sequences,
                    const std::vector<float> &cp_rotations, const uint32_t sample_idx,
                    const uint32_t max_path_depth, FirstHitInfo &first_hit_info)
{
    glm::vec3 attenuation_accum = {1.0f, 1.0f, 1.0f};
    glm::vec3 output_color = {0.0f, 0.0f, 0.0f};
    for (uint32_t path_depth = 0; path_depth < max_path_depth; ++path_depth) {
        spt::HitInfo hit_info{};
        if (hit_anything(scene, world_ray, hit_info)) {
            const float rnd_bsdf_x = glm::fract(
                get_bounce_rng_sample(sample_idx, path_depth, RNG_IDX_BSDF_X, rng_sequences) +
                cp_rotations.at(RNG_NUM_BASE_IDX + RNG_IDX_BSDF_X));
            const float rnd_bsdf_y = glm::fract(
                get_bounce_rng_sample(sample_idx, path_depth, RNG_IDX_BSDF_Y, rng_sequences) +
                cp_rotations.at(RNG_NUM_BASE_IDX + RNG_IDX_BSDF_Y));
            const float rnd_fresnel = glm::fract(
                get_bounce_rng_sample(sample_idx, path_depth, RNG_IDX_FRESNEL, rng_sequences) +
                cp_rotations.at(RNG_NUM_BASE_IDX + RNG_IDX_FRESNEL));

            const spt::Material material = scene.materials[hit_info.material_id];

            if (path_depth == 0) {
                first_hit_info.albedo = material.albedo;
                first_hit_info.world_normal = hit_info.normal;
            }

            const glm::vec3 emitted = spt::material_emit(material);
            output_color += attenuation_accum * emitted;

            spt::Ray scattered{};
            glm::vec3 attenuation{};
            if (spt::material_scatter(material, world_ray, hit_info, rnd_bsdf_x, rnd_bsdf_y,
                                      rnd_fresnel, attenuation, scattered)) {
                world_ray = scattered;
                world_ray.t_min = 0.0005f;
                world_ray.t_max = 1e6f;

                attenuation_accum *= attenuation;
            }
            else {
                break;
            }
        }
        else {
            output_color += attenuation_accum * spt::sample_sky_light(scene.sky_light, world_ray);
            break;
        }
    }

    return output_color;
}

int main()
{
    Settings settings;

    Scene scene;
    initialize_scene(scene, settings.scene_id);

    spt::Camera camera{};
    initialize_camera(camera, settings.scene_id, settings.width, settings.height);

    const uint32_t num_rng_sequences =
        RNG_NUM_BASE_IDX + (settings.max_path_depth + 2) * RNG_NUM_BOUNCE_IDX;
    std::vector<std::vector<float>> rng_sequences =
        spt::generate_permuted_halton_sequences(num_rng_sequences, settings.spp, 75313);

    std::vector<glm::vec3> color_image(settings.width * settings.height);
    std::vector<glm::vec3> albedo_image(settings.width * settings.height);
    std::vector<glm::vec3> normal_image(settings.width * settings.height);
    std::vector<glm::vec3> normal_01_image(settings.width * settings.height);

    // Start the path tracing
    const double tic = omp_get_wtime();

    #pragma omp parallel for schedule(dynamic, 1)
    for (int32_t j = settings.height - 1; j >= 0; --j) {
        for (int32_t i = 0; i < settings.width; ++i) {
            // Generate an extra set of random values that are used for applying a per-pixel shift
            // (Cranley-Patterson rotation) on the RNG sequences. This prevents correlation when all
            // pixels draw samples from the same RNG sequences.
            const std::vector<float> cp_rotations =
                spt::hash3d_sequence(RNG_NUM_BASE_IDX + RNG_NUM_BOUNCE_IDX, glm::vec3(i, j, 0.0f));

            glm::vec3 color = {0.0f, 0.0f, 0.0f};
            glm::vec3 albedo = {0.0f, 0.0f, 0.0f};
            glm::vec3 normal = {0.0f, 0.0f, 0.0f};

            for (uint32_t sample_idx = 0; sample_idx < settings.spp; ++sample_idx) {
                const float u = float(i) / settings.width;
                const float v = float(j) / settings.height;

                const float rnd_aa_x =
                    glm::fract(get_base_rng_sample(sample_idx, RNG_IDX_AA_X, rng_sequences) +
                               cp_rotations.at(RNG_IDX_AA_X));
                const float rnd_aa_y =
                    glm::fract(get_base_rng_sample(sample_idx, RNG_IDX_AA_Y, rng_sequences) +
                               cp_rotations.at(RNG_IDX_AA_Y));
                const float rnd_lens_x =
                    glm::fract(get_base_rng_sample(sample_idx, RNG_IDX_LENS_X, rng_sequences) +
                               cp_rotations.at(RNG_IDX_LENS_X));
                const float rnd_lens_y =
                    glm::fract(get_base_rng_sample(sample_idx, RNG_IDX_LENS_Y, rng_sequences) +
                               cp_rotations.at(RNG_IDX_LENS_Y));

                const float aa_offset_x = (rnd_aa_x - 0.5f) / settings.width;
                const float aa_offset_y = (rnd_aa_y - 0.5f) / settings.height;

                spt::Ray ray = spt::get_camera_ray(camera, u + aa_offset_x, v + aa_offset_y,
                                                   rnd_lens_x, rnd_lens_y);
                ray.t_min = 0.0005f;
                ray.t_max = 1e6f;

                FirstHitInfo first_hit_info{};
                color += trace_ray(ray, scene, rng_sequences, cp_rotations, sample_idx,
                                   settings.max_path_depth, first_hit_info);
                albedo += first_hit_info.albedo;
                normal += first_hit_info.world_normal;
            }

            color /= float(settings.spp);
            albedo /= float(settings.spp);
            normal /= float(settings.spp);

            color *= settings.exposure;

            const int32_t pixel_index = i + j * settings.width;
            color_image[pixel_index] = color;
            albedo_image[pixel_index] = albedo;
            normal_image[pixel_index] = normal;
            normal_01_image[pixel_index] = 0.5f * normal + 0.5f;
        }
    }

    const double toc = omp_get_wtime();
    LOG_INFO("Elapsed time (s): %f\n", toc - tic);

    // Apply tone mapping and gamma correction
    color_image = spt::tonemap_aces_filter(color_image);
    color_image = spt::lin2srgb_filter(color_image);

    // Export the path tracing result and auxiliary images to PPM and PFM files
    spt::write_ppm("output.ppm", color_image, settings.width, settings.height);
    spt::write_ppm("albedo.ppm", albedo_image, settings.width, settings.height);
    spt::write_ppm("normal.ppm", normal_01_image, settings.width, settings.height);

    spt::write_pfm("color.pfm", color_image, settings.width, settings.height);
    spt::write_pfm("albedo.pfm", albedo_image, settings.width, settings.height);
    spt::write_pfm("normal.pfm", normal_image, settings.width, settings.height);

    return EXIT_SUCCESS;
}
