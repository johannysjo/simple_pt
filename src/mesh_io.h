/// @file
/// @brief Mesh IO.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace spt {

void load_obj_mesh(const char *filename, std::vector<glm::vec3> &vertices,
                   std::vector<uint32_t> &indices);

} // namespace spt
