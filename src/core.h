/// @file
/// @brief Path tracing core functions and structs.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace spt {

struct Ray {
    glm::vec3 origin;
    glm::vec3 direction;
    float t_min;
    float t_max;
};

Ray transform_ray(const Ray &ray, const glm::mat4 &transform);

struct Camera {
    glm::vec3 forward;
    glm::vec3 side;
    glm::vec3 up;
    glm::vec3 origin;
    glm::vec3 lower_left_corner;
    glm::vec3 horizontal;
    glm::vec3 vertical;
    float lens_radius;
};

void camera_init(Camera &camera, const glm::vec3 &eye, const glm::vec3 &up, const glm::vec3 &center,
                 float vfov, float aspect, float aperture, float focus_dist);

Ray get_camera_ray(const Camera &camera, float x, float y, float rnd_lens_x, float rnd_lens_y);

struct HitInfo {
    float t;
    glm::vec3 p;
    glm::vec3 normal;
    uint32_t material_id;
};

struct Sphere {
    glm::vec3 center;
    float radius;
    uint32_t material_id;
};

bool sphere_hit(const Sphere &sphere, const Ray &ray, float t_min, float t_max, HitInfo &hit_info);

struct Cube {
    glm::vec3 center;
    float size;
    uint32_t material_id;
};

glm::vec3 cube_get_normal_at_point(const Cube &cube, const glm::vec3 &p);

bool cube_hit(const Cube &cube, const Ray &ray, float t_min, float t_max, HitInfo &hit_info);

struct AABB {
    glm::vec3 min_corner;
    glm::vec3 max_corner;
};

bool aabb_hit(const AABB &aabb, const Ray &ray, float t_min, float t_max);

struct TriangleMesh {
    std::vector<glm::vec3> vertices;
    std::vector<uint32_t> indices;
};

bool triangle_hit(const glm::vec3 &v0, const glm::vec3 &v1, const glm::vec3 &v2, const Ray &ray,
                  float t_min, float t_max, HitInfo &hit_info);

bool triangle_mesh_hit(const TriangleMesh &mesh, const Ray &ray, float t_min, float t_max,
                       HitInfo &hit_info);

AABB compute_aabb(const std::vector<glm::vec3> &vertices);

struct TriangleMeshInstances {
    std::vector<uint32_t> mesh_id;
    std::vector<glm::mat4> world_from_model;
    std::vector<glm::mat4> model_from_world;
    std::vector<uint32_t> material_id;
};

enum MaterialType {
    MATERIAL_TYPE_MATTE,
    MATERIAL_TYPE_METAL,
    MATERIAL_TYPE_GLASS,
    MATERIAL_TYPE_GLOSSY,
    MATERIAL_TYPE_EMISSIVE
};

struct Material {
    MaterialType type;
    glm::vec3 albedo;
    float roughness;
    float refraction_index;
    float intensity;
};

bool material_scatter(const Material &material, const Ray &ray, const HitInfo &hit_info,
                      float rnd_bsdf_x, float rnd_bsdf_y, float rnd_fresnel, glm::vec3 &attenuation,
                      Ray &scattered);

glm::vec3 material_emit(const Material &material);

struct HemisphereLight {
    glm::vec3 ground_color;
    glm::vec3 sky_color;
};

glm::vec3 sample_hemisphere_light(const HemisphereLight &light, const Ray &world_ray);

struct PixelBuffer {
    std::vector<glm::vec3> pixels;
    uint32_t width;
    uint32_t height;
};

glm::vec3 sample_envmap(const std::vector<glm::vec3> &image, const uint32_t width,
                        const uint32_t height, const glm::vec3 &ray_dir);

enum SkyLightType { SKY_LIGHT_TYPE_HEMISPHERE, SKY_LIGHT_TYPE_ENVMAP, SKY_LIGHT_TYPE_NONE };

struct SkyLight {
    SkyLightType type;
    HemisphereLight hemisphere_light;
    PixelBuffer envmap;
    float intensity;
};

glm::vec3 sample_sky_light(const SkyLight &sky_light, const Ray &world_ray);

} // namespace spt
