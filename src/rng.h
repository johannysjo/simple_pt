/// @file
/// @brief Random number generators.
///
/// @section LICENSE
///
/// Copyright (C) 2020  Johan Nysjö
///
/// This software is distributed under the MIT license. See the
/// included LICENSE.txt file for details.

#include <glm/vec3.hpp>

#include <stdint.h>
#include <vector>

namespace spt {

std::vector<std::vector<float>>
generate_permuted_halton_sequences(uint32_t num_sequences, uint32_t num_samples, uint32_t seed);

std::vector<std::vector<float>> generate_lcg_sequences(uint32_t num_sequences, uint32_t num_samples,
                                                       uint32_t seed);

std::vector<float> hash3d_sequence(uint32_t num_samples, const glm::vec3 &seed);

} // namespace spt
