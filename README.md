# SimplePT
My somewhat neglected pet path tracer that I started to implement in 2016 after reading Peter Shirley's excellent Ray Tracing in One Weekend book. It supports rendering of analytic shapes (spheres or boxes) and instanced triangle meshes, with emissive objects or HDR environment maps as light sources. The renderer runs on the CPU and has no acceleration structures except bounding boxes, so rendering meshes is... really slow. But it works.

Supported features:

- Rendering of analytic shapes (spheres or boxes) and instanced triangle meshes (loaded from .obj files)
- Matte, glossy, metal, glass, and emissive materials
- Low-discrepancy sampling with digit-permuted Halton sequences. A per-pixel shift (Cranley-Patterson rotation) is applied on the sequences to randomize them between pixels.
- Sampling of equirectangular HDR environment maps (.hdr images)
- Tone mapping
- Multi-threading via OpenMP
- Export of PPM and PFM images
- Export of auxiliary images (albedo and normals) for denoising

## Example screenshot
![Screenshot](https://bitbucket.org/johannysjo/simple_pt/raw/master/screenshots/screenshot.png "Screenshot")

## Compiling (Linux)
Clone the git repository as
```bash
$ git clone --recursive https://bitbucket.org/johannysjo/simple_pt.git
```
and run (from the root folder)
```bash
$ ./build.sh
```
Requires CMake 3.0 or higher and a C++11 capable compiler (GCC 4.6+).

## Usage
Use the build script to compile and run the renderer. The renderer exports three images: color.pfm, albedo.pfm, and normal.pfm. color.pfm is the tone mapped and gamma corrected path tracing result, albedo.pfm is the material albedo image, and normal.pfm is the world space normal image. These three PFM images can then be used as input to Intel's Open Image Denoiser library (not included, but can be downloaded [here](https://www.openimagedenoise.org/downloads.html)) to remove remaining noise from the image, like so:
```bash
$ ./oidnDenoise --ldr color.pfm --albedo albedo.pfm --normal normal.pfm --srgb --output output.ppm
```
See example screenshots below. The renderer also exports PPM versions of the images for preview.

Currently there is no interface or support for importing new scenes. The renderer has three hardcoded example scenes (random spheres, Cornell box, bunnies) that can be changed with a scene_id variable in main.cpp.

## License
SimplePT is provided under the MIT license. See LICENSE.txt for more information.

## More screenshots
Denoising of the Cornell box scene:

![Denoising Cornell box](https://bitbucket.org/johannysjo/simple_pt/raw/master/screenshots/screenshot_denoising_cornell_box.png)

Denoising of the random spheres scene:

![Denoising random spheres](https://bitbucket.org/johannysjo/simple_pt/raw/master/screenshots/screenshot_denoising_sphere_scene.png)

Instanced lowpoly bunny meshes (207 instances, colored by instance ID):

![Instanced bunnies](https://bitbucket.org/johannysjo/simple_pt/raw/master/screenshots/screenshot_instanced_bunnies_golden_ratio_colors.png)
