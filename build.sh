#!/bin/bash

export SIMPLE_PT_ROOT=$(pwd)

# Generate build script
cd $SIMPLE_PT_ROOT && \
if [ ! -d build ]; then
    mkdir build
fi
cd build && \
cmake -DCMAKE_INSTALL_PREFIX=$SIMPLE_PT_ROOT ../ && \

# Build program
make clean && \
make && \

# Run program and display output image
cd .. && \
build/simple_pt && \
display output.ppm
